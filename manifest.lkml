constant: connection {
  value: "keboola_block_crm_hubspot"
}

# your Hubspot account_id for object links
constant: hubspot_account_id {
  value: "505715"
}
